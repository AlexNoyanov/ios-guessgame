//
//  GuessTheNumberApp.swift
//  GuessTheNumber
//
//  Created by Alex Noyanov on 07.02.2024.
//

import SwiftUI

@main
struct GuessTheNumberApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
